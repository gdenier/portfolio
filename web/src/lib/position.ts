export const getPosition = (el) => {
    let coord = {x:0, y:0}

    if (el) {
        let rect=el.getBoundingClientRect();
        coord.x = rect.left + window.scrollX;
        coord.y = rect.top + window.scrollY;
    }

    return coord
}