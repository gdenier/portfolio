import Skill from './Skill'
import Image from './values/Image'
import { Type } from 'class-transformer'

export default class Project {
    title: string
    description: string
    finished: boolean
    team: number

    @Type(() => Date)
    from: Date

    @Type(() => Date)
    to: Date

    @Type(() => Image)
    cover: Image

    @Type(() => Image)
    images: Image[]

    @Type(() => Skill)
    skills: Skill[]
}
