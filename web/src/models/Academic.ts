import Image from './values/Image'
import { Type } from 'class-transformer'

export default class Academic {
    degree: string
    schoolName: string

    @Type(() => Date)
    from: Date

    @Type(() => Date)
    to: Date

    @Type(() => Image)
    schoolLogo: Image
}
