export default class Image {
    id: string
    url: string
    alternativeText: string
}
