import SkillCategory from './values/SkillCategory'
import Image from './values/Image'
import { Type } from 'class-transformer'
import SkillType from './values/SkillType'

export default class Skill {
    name: string

    @Type(() => SkillCategory)
    categories: SkillCategory[]

    @Type(() => SkillType)
    type: SkillType

    @Type(() => Image)
    logo: Image
}
