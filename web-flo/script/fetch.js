var fs = require('fs')
var fetch = require('node-fetch')

fetch('http://localhost:1337/graphql', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        query: `
                query getPeople($name: String!) {
                    people(where: { firstname: $name }) {
                        firstname
                        lastname
                        catchPhrase
                        email
                        title
                        city
                        profil {
                            url
                            alternativeText
                        }
                        jobs {
                            title
                            from
                            to
                            description
                            company {
                                name
                                logo {
                                    url
                                    alternativeText
                                }
                            }
                        }
                        projects {
                            id
                            title
                            description
                            from
                            to
                            cover {
                                url
                                alternativeText
                            }
                            images {
                                url
                                alternativeText
                            }
                            finished
                            team
                            companies {
                                name
                                logo {
                                    url
                                    alternativeText
                                }
                            }
                            skills {
                                name
                                logo {
                                    url
                                    alternativeText
                                }
                                categories {
                                    name
                                }
                            }
                        }
                        values {
                            name
                            icon
                        }
                        skills {
                            name
                            logo {
                                url
                                alternativeText
                            }
                            categories {
                                name
                            }
                            type {
                                name
                            }
                        }
                        academics {
                            degree
                            schoolName
                            from
                            to
                            schoolLogo {
                                url
                                alternativeText
                            }
                        }
                    }
                }
            `,
        variables: {
            name: 'guillaume'
        }
    })
})
    .then(res => res.json())
    .then(json =>
        fs.writeFile(
            './src/data/person.json',
            JSON.stringify(json.data),
            () => {}
        )
    )
