module.exports = {
    purge: [],
    theme: {
        container: {
            center: true
        },
        borderRadius: {
            DEFAULT: '15px',
            full: '9999px'
        },
        boxShadow: {
            DEFAULT: '2px 2px 20px rgba(0, 0, 0, 0.05)'
        },
        fontFamily: {
            sans: ['Lato'],
            serif: ['Lato'],
            mono: ['Lato'],
            display: ['Lato'],
            body: ['Lato']
        },
        fontSize: {
            h1: [
                '55px',
                {
                    letterSpacing: '5%'
                }
            ],
            h2: [
                '36px',
                {
                    letterSpacing: '5%'
                }
            ],
            h3: '24px',
            amphase: [
                '18px',
                {
                    letterSpacing: '5%',
                    lineHeight: '160%'
                }
            ],
            p: [
                '16px',
                {
                    letterSpacing: '5%',
                    lineHeight: '160%'
                }
            ]
        },
        extend: {
            colors: {
                blue: {
                    DEFAULT: '#3A808A',
                    dark: '#284E5B'
                },
                orange: '#FFA432',
                beige: {
                    DEFAULT: '#E4DDCA',
                    light: '#F5F0EC'
                }
            },
            height: {
                '128': '32rem'
            },
            backgroundImage: theme => ({
                intro: "url('/img/intro.svg')"
            }),
            zIndex: {
                '-10': '-10'
            },
            height: {
                '95': '95%'
            },
            inset: {
                screen: '100vh'
            },
            transitionProperty: {
                display: 'display'
            }
        }
    },
    variants: {
        extend: {
            display: ['hover', 'focus']
        }
    },
    plugins: []
}
