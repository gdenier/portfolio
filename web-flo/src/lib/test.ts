export interface Coord {
  x: number
  y: number
}

export const getPosition = (el): Coord => {
  let coord: Coord = {x:0, y:0}
  while (el) {
    if (el.tagName == "BODY") {
      // deal with browser quirks with body/window/document and page scroll
      var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
      var yScroll = el.scrollTop || document.documentElement.scrollTop;
      coord.x += (el.offsetLeft - xScroll + el.clientLeft);
      coord.y += (el.offsetTop - yScroll + el.clientTop);
    } else {
      // for all other non-BODY elements
      coord.x += (el.offsetLeft - el.scrollLeft + el.clientLeft);
      coord.y += (el.offsetTop - el.scrollTop + el.clientTop);
    }
    el = el.offsetParent;
  }
  return coord;
}