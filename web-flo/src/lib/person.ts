import { writable } from 'svelte/store'
import Person from 'models/Person'

export const person = writable(new Person())