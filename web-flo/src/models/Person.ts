import Image from './values/Image'
import Value from './Value'
import Skill from './Skill'
import Job from './Job'
import Project from './Project'
import Academic from './Academic'
import { Type } from 'class-transformer'

export default class Person {
    firstname: string
    lastname: string
    catchPhrase: string
    title: string
    email: string
    linkedin: string
    git: string
    city: string

    @Type(() => Image)
    profil: Image

    @Type(() => Value)
    values: Value[] = []

    @Type(() => Skill)
    skills: Skill[] = []

    @Type(() => Job)
    jobs: Job[] = []

    @Type(() => Academic)
    academics: Academic[] = []

    @Type(() => Project)
    projects: Project[] = []
}
