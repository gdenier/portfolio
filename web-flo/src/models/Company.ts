import Image from './values/Image'
import { Type } from 'class-transformer'

export default class Company {
    name: string

    @Type(() => Image)
    logo: Image
}
