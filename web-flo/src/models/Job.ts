import Company from './Company'
import { Type } from 'class-transformer'

export default class Job {
    title: string
    description: string

    @Type(() => Date)
    from: Date

    @Type(() => Date)
    to: Date

    @Type(() => Company)
    company: Company
}
